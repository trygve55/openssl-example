#include "crypto.hpp"
#include <iostream>

using namespace std;

void genText(std::string const &chars, size_t max_len, string const &cur, string const &hash) {
  if (cur.length() == max_len) {
    return;
  } else {
    for (auto c : chars) {
      std::string next = cur + c;

      if (Crypto::hex(Crypto::pbkdf2(next, "Saltet til Ola", 2048, 160)).compare(hash) == 0) {
        cout << "The password is \"" << next << "\"." << endl;

        return;
      }

      genText(chars, max_len, next, hash);
    }
  }
}

int main() {
  char hash[] = "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";

  char chars[] =
      "0123456789"
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
      "abcdefghijklmnopqrstuvwxyz";

  genText(chars, 3, "", hash);
}
